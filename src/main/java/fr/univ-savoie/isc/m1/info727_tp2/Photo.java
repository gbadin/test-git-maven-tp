package isc_m1_fw_gb.info727_tp2;

/***********************************************************************
 * Module:  Photo.java
 * Author:  Gr�goire Badin - Fabien Way
 * Purpose: Defines the Class Photo
 ***********************************************************************/

public class Photo {
	private String nom;
	private byte[] data;
	
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the data
	 */
	public byte[] getData() {
		return data;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(byte[] data) {
		this.data = data;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Photo [nom=" + nom + "]";
	}
	

}