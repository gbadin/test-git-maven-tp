package isc_m1_fw_gb.info727_tp2;

/***********************************************************************
 * Module:  Historien.java
 * Author:  Gr�goire Badin - Fabien Way
 * Purpose: Defines the Class Historien
 ***********************************************************************/

import java.util.*;

public class Historien extends Utilisateur {

	private java.util.ArrayList<Demande> demandes;

	public ArrayList<Demande> consulterDemandes() throws NotAuthorizedException{
		return SystemeLouvre.getInstance().recupererDemandes();
	}

	public Demande creationDemande() throws NotAuthorizedException{
		Demande demande = new Demande();
		this.addDemande(demande);
		return demande;
	}

	public ArrayList<Integer> getActionDisponibles() {
		ArrayList<Integer> toReturn = new ArrayList<Integer>();
		toReturn.add(Utilisateur.ACTION_CONSULTER_DEMANDES);
		toReturn.add(Utilisateur.ACTION_CONSULTER_SES_DEMANDES);
		toReturn.add(Utilisateur.ACTION_CREER_DEMANDE);
		toReturn.add(Utilisateur.ACTION_VALIDER_DEMANDE);
		return toReturn;
	}
	
	/** @pdGenerated default getter */
	public java.util.List<Demande> getDemandes() {
		if (demandes == null)
			demandes = new java.util.ArrayList<Demande>();
		return demandes;
	}

	/** @pdGenerated default iterator getter */
	public java.util.Iterator<Demande> getIteratorDemandes() {
		if (demandes == null)
			demandes = new java.util.ArrayList<Demande>();
		return demandes.iterator();
	}

	/**
	 * @pdGenerated default setter
	 * @param newDemandes
	 */
	public void setDemandes(java.util.List<Demande> newDemandes) {
		removeAllDemandes();
		for (java.util.Iterator<Demande> iter = newDemandes.iterator(); iter.hasNext();)
			addDemande(iter.next());
	}

	/**
	 * @pdGenerated default add
	 * @param newDemande
	 */
	public void addDemande(Demande newDemande) {
		if (newDemande == null)
			return;
		if (this.demandes == null)
			this.demandes = new java.util.ArrayList<Demande>();
		if (!this.demandes.contains(newDemande)) {
			this.demandes.add(newDemande);
			newDemande.setHistorien(this);
		}
	}

	/**
	 * @pdGenerated default remove
	 * @param oldDemande
	 */
	public void removeDemande(Demande oldDemande) {
		if (oldDemande == null)
			return;
		if (this.demandes != null)
			if (this.demandes.contains(oldDemande)) {
				this.demandes.remove(oldDemande);
				oldDemande.setHistorien((Historien) null);
			}
	}

	/** @pdGenerated default removeAll */
	public void removeAllDemandes() {
		if (demandes != null) {
			Demande oldDemande;
			for (java.util.Iterator<Demande> iter = getIteratorDemandes(); iter
					.hasNext();) {
				oldDemande = iter.next();
				iter.remove();
				oldDemande.setHistorien((Historien) null);
			}
		}
	}

	public String toString() {
		return "Historien : " + getNom();
	}

	@Override
	public ArrayList<Demande> recupererSesDemandes() throws NotAuthorizedException{
		return demandes;
	}

	@Override
	public void prendreEnChargeDemande(Demande demande) throws NotAuthorizedException{
		throw new NotAuthorizedException("Un Historien ne peut pas prendre en charge une demande.");
	}

	@Override
	public void validerDemande(Demande demande) throws NotAuthorizedException{
		if(demandes.contains(demande))
		{
			if(!demande.valider())
				throw new NotAuthorizedException("Un historien ne peut pas valider une demande qui n'est pas compl�t�e");
		}
		else
			throw new NotAuthorizedException("Un historien ne peut pas valider une demande qui n'est pas la sienne");
	}

	@Override
	public void completerDemande(Demande demande, ArrayList<Photo> listePhotos) throws NotAuthorizedException{
		throw new NotAuthorizedException("Un Historien ne peut pas prendre en charge une demande.");
	}
}