package isc_m1_fw_gb.info727_tp2;

/***********************************************************************
 * Module:  NotAuthorizedException.java
 * Author:  Gr�goire Badin - Fabien Way
 * Purpose: Defines the Class NotAuthorizedException
 ***********************************************************************/


public class NotAuthorizedException extends Exception
{

		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

		public NotAuthorizedException(String string)
		{
			super(string);
		}
	}