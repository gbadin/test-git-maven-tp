package isc_m1_fw_gb.info727_tp2;
/***********************************************************************
 * Module:  Musee.java
 * Author:  Gr�goire Badin - Fabien Way
 * Purpose: Defines the Class Musee
 ***********************************************************************/

public class Musee implements Photographiable {
   private String nom;
   
   public String getNom() {
	return nom;
}


public void setNom(String nom) {
	this.nom = nom;
}

   private java.util.ArrayList<Salle> salles;
   
   public String recupereResume() {
	   return "Musee : "+nom;
   }
   
   /** @pdGenerated default getter */
   public java.util.List<Salle> getSalles() {
      if (salles == null)
         salles = new java.util.ArrayList<Salle>();
      return salles;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator<Salle> getIteratorSalles() {
      if (salles == null)
         salles = new java.util.ArrayList<Salle>();
      return salles.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newSalles */
   public void setSalles(java.util.List<Salle> newSalles) {
      removeAllSalles();
      for (java.util.Iterator<Salle> iter = newSalles.iterator(); iter.hasNext();)
         addSalle(iter.next());
   }
   
   /** @pdGenerated default add
     * @param newSalle */
   public void addSalle(Salle newSalle) {
      if (newSalle == null)
         return;
      if (this.salles == null)
         this.salles = new java.util.ArrayList<Salle>();
      if (!this.salles.contains(newSalle))
      {
         this.salles.add(newSalle);
         newSalle.setMusee(this);      
      }
      newSalle.setMusee(this);
   }
   
   /** @pdGenerated default remove
     * @param oldSalle */
   public void removeSalle(Salle oldSalle) {
      if (oldSalle == null)
         return;
      if (this.salles != null)
         if (this.salles.contains(oldSalle))
         {
            this.salles.remove(oldSalle);
            oldSalle.setMusee((Musee)null);
         }
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllSalles() {
      if (salles != null)
      {
         Salle oldSalle;
         for (java.util.Iterator<Salle> iter = getIteratorSalles(); iter.hasNext();)
         {
            oldSalle = iter.next();
            iter.remove();
            oldSalle.setMusee((Musee)null);
         }
      }
   }

}