package isc_m1_fw_gb.info727_tp2;

/***********************************************************************
 * Module:  EtatCloture.java
 * Author:  Gr�goire Badin - Fabien Way
 * Purpose: Defines the Class EtatCloture
 ***********************************************************************/

public class EtatCloture extends EtatDemande {
	
   public EtatCloture(Demande demande)
   {
		super(demande);
   }

/** @param p */
   public boolean prendreEnCharge(Photographe p) {
      return false;
   }
   
   public boolean completer() {
      return false;
   }
   
   public boolean valider() {
      return false;
   }
   
   public String toString() {
	   String toStr = "Demande cl�tur�e" 
			   + ", photos : ";
	   
		if (! demande.getPhotos().isEmpty())
			for (Photo photoFor : demande.getPhotos()){
				toStr += photoFor.toString() + "	";
			}
		else
			toStr += "aucune";
   
		return toStr;
   }

}