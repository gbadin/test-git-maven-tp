package isc_m1_fw_gb.info727_tp2;

public interface Observer
{
	public abstract void update();
}
