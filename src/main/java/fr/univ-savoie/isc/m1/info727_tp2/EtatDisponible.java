package isc_m1_fw_gb.info727_tp2;

/***********************************************************************
 * Module:  EtatDisponible.java
 * Author:  Gr�goire Badin - Fabien Way
 * Purpose: Defines the Class EtatDisponible
 ***********************************************************************/

public class EtatDisponible extends EtatDemande
{
	
   public EtatDisponible(Demande demande)
   {
		super(demande);
   }

/** @param p */
   public boolean prendreEnCharge(Photographe p)
   {
      demande.setPhotographe(p);
      p.addListeDemande(demande);
      demande.setEtat(new EtatAttribue(demande));
      return true;
   }
   
   public boolean completer()
   {
      return false;
   }
   
   public boolean valider()
   {
      return false;
   }
   
   public String toString() {
	   return "Demande disponible";
   }

}