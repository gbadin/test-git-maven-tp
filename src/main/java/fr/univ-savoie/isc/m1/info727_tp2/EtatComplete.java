package isc_m1_fw_gb.info727_tp2;

/***********************************************************************
 * Module:  EtatComplete.java
 * Author:  Gr�goire Badin - Fabien Way
 * Purpose: Defines the Class EtatComplete
 ***********************************************************************/

public class EtatComplete extends EtatDemande {
   
	public EtatComplete(Demande demande)
	{
		super(demande);
	}

	/** @param p */
   public boolean prendreEnCharge(Photographe p) {
      return false;
   }
   
   public boolean completer() {
      return false;
   }
   
   public boolean valider() {
	   demande.ajouterPhotosAuCataogue();
	   demande.setEtat(new EtatCloture(demande));
	   return true;
   }

   public String toString() {
	   String toStr = "Demande compl�t�e par le photographe " 
			   + demande.getPhotographe().getNom()
			   + ", photos : ";
	   
		if (! demande.getPhotos().isEmpty())
			for (Photo photoFor : demande.getPhotos()){
				toStr += photoFor.toString() + "	";
			}
		else
			toStr += "aucune";
   
		return toStr;
   }

}