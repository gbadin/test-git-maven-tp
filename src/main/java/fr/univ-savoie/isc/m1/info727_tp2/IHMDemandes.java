package isc_m1_fw_gb.info727_tp2;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class IHMDemandes implements Observer
{
	private SystemeLouvre subject;

	private JFrame fFenetre;
	private JLabel lNombreDemandes;
	
	public IHMDemandes()
	{
		super();
		subject = SystemeLouvre.getInstance();
		subject.attach(this);
		fFenetre = new JFrame("Mus�e du louve");
		fFenetre.setSize(new Dimension(200,100));
		lNombreDemandes = new JLabel();
		update();
		fFenetre.add(lNombreDemandes);
		fFenetre.setVisible(true);
	}
	
	@Override
	public void update()
	{
		lNombreDemandes.setText("Nombre de demandes : " + subject.getDemandes().size());
	}
}
