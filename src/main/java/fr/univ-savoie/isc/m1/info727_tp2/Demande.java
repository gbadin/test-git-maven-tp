package isc_m1_fw_gb.info727_tp2;

/***********************************************************************
 * Module:  Demande.java
 * Author:  Gr�goire Badin - Fabien Way
 * Purpose: Defines the Class Demande
 ***********************************************************************/

import java.util.*;

public class Demande {
  
private String utilisation;
   private String commentaire;
   
   private Photographiable photographiable;
   private EtatDemande etat;
   private java.util.ArrayList<Photo> photos;
   private Historien historien;
   private Photographe photographe;
   
   /** @param historien 
    * @param cible 
    * @param utilisation 
    * @param commentaire */
   public void remplir(Historien historien, Photographiable cible, String utilisation, String commentaire) {
	   photographiable = cible;
	   this.historien = historien;
	   this.setUtilisation(utilisation);
	   this.setCommentaire(commentaire);
	   etat = new EtatDisponible(this);
   }
   
   /** @param photographe */
   public boolean prendreEnCharge(Photographe photographe)
   {
      return etat.prendreEnCharge(photographe);
   }
   
   /** @param photographe */
   public void setPhotographe(Photographe photographe) {
      this.photographe = photographe;
   }
   
   /** @param photographe 
    * @param listePhotos */
   public boolean completer(ArrayList<Photo> listePhotos)
   {
	  if(etat.completer())
	  {
		  photos = listePhotos;
		  return true;
	  }
	  else 
		  return false;
   }
   
   public boolean valider() {
      return etat.valider();
   }
   
   public void ajouterPhotosAuCataogue()
   {
	   for(Photo photo : photos)
		   SystemeLouvre.getInstance().addPhotos(photo);
   }
   
   /** @param etat */
   public void setEtat(EtatDemande etat) {
      this.etat = etat;
      this.etat.setDemande(this);
   }
   
   
   /**
 * @return the utilisation
 */
public String getUtilisation() {
	return utilisation;
}

public String toString() {
	String toStr = "Demande [utilisation : " + utilisation 
					+ ", commentaire : " + commentaire
					+ ", photographiable : " + photographiable.recupereResume()
					+ ", " + historien
					+ ", etat : " + etat
					+ "]";
	return toStr;
}

/**
 * @param utilisation the utilisation to set
 */
public void setUtilisation(String utilisation) {
	this.utilisation = utilisation;
}

/**
 * @return the commentaire
 */
public String getCommentaire() {
	return commentaire;
}

/**
 * @param commentaire the commentaire to set
 */
public void setCommentaire(String commentaire) {
	this.commentaire = commentaire;
}

/** @pdGenerated default parent getter */
   public Photographiable getPhotographiable() {
      return photographiable;
   }
   
   /** @pdGenerated default parent setter
     * @param newPhotographiable */
   public void setPhotographiable(Photographiable newPhotographiable) {
      this.photographiable = newPhotographiable;
   }
   /** @pdGenerated default parent getter */
   public EtatDemande getEtat() {
      return etat;
   }
   /** @pdGenerated default getter */
   public java.util.List<Photo> getPhotos() {
      if (photos == null)
         photos = new java.util.ArrayList<Photo>();
      return photos;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator<Photo> getIteratorPhotos() {
      if (photos == null)
         photos = new java.util.ArrayList<Photo>();
      return photos.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newPhotos */
   public void setPhotos(java.util.List<Photo> newPhotos) {
      removeAllPhotos();
      for (java.util.Iterator<Photo> iter = newPhotos.iterator(); iter.hasNext();)
         addPhotos(iter.next());
   }
   
   /** @pdGenerated default add
     * @param newPhoto */
   public void addPhotos(Photo newPhoto) {
      if (newPhoto == null)
         return;
      if (this.photos == null)
         this.photos = new java.util.ArrayList<Photo>();
      if (!this.photos.contains(newPhoto))
         this.photos.add(newPhoto);
   }
   
   /** @pdGenerated default remove
     * @param oldPhoto */
   public void removePhotos(Photo oldPhoto) {
      if (oldPhoto == null)
         return;
      if (this.photos != null)
         if (this.photos.contains(oldPhoto))
            this.photos.remove(oldPhoto);
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllPhotos() {
      if (photos != null)
         photos.clear();
   }
   /** @pdGenerated default parent getter */
   public Historien getHistorien() {
      return historien;
   }
   
   /** @pdGenerated default parent setter
     * @param newHistorien */
   public void setHistorien(Historien newHistorien) {
      if (this.historien == null || !this.historien.equals(newHistorien))
      {
         if (this.historien != null)
         {
            Historien oldHistorien = this.historien;
            this.historien = null;
            oldHistorien.removeDemande(this);
         }
         if (newHistorien != null)
         {
            this.historien = newHistorien;
            this.historien.addDemande(this);
         }
      }
   }
   /** @pdGenerated default parent getter */
   public Photographe getPhotographe() {
      return photographe;
   }
   
   @Override
  	public boolean equals(Object arg0) {
  		if(arg0 instanceof Demande)
  			return this == (Demande)arg0;
  		return false;
  	}

}