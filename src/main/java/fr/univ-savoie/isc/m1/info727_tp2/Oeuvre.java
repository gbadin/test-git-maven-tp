package isc_m1_fw_gb.info727_tp2;

/***********************************************************************
 * Module:  Oeuvre.java
 * Author:  Gr�goire Badin - Fabien Way
 * Purpose: Defines the Class Oeuvre
 ***********************************************************************/

public class Oeuvre implements Photographiable {
   private String nom;
   
   public String getNom() {
	return nom;
}


public void setNom(String nom) {
	this.nom = nom;
}

/** @pdRoleInfo migr=no name=Salle assc=exposeDans mult=0..1 */
   public Salle salle;
   
   public String recupereResume() {
      return "Oeuvre : "+nom;
   }
   
   
   /** @pdGenerated default parent getter */
   public Salle getSalle() {
      return salle;
   }
   
   /** @pdGenerated default parent setter
     * @param newSalle */
   public void setSalle(Salle newSalle) {
      if (this.salle == null || !this.salle.equals(newSalle))
      {
         if (this.salle != null)
         {
            Salle oldSalle = this.salle;
            this.salle = null;
            oldSalle.removeOeuvreExposee(this);
         }
         if (newSalle != null)
         {
            this.salle = newSalle;
            this.salle.addOeuvreExpose(this);
         }
      }
   }

}