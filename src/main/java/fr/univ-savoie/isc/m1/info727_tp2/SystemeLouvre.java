package isc_m1_fw_gb.info727_tp2;

/***********************************************************************
 * Module:  SystemeLouvre.java
 * Author:  Gr�goire Badin - Fabien Way
 * Purpose: Defines the Class SystemeLouvre
 ***********************************************************************/

import java.util.*;

public class SystemeLouvre extends Subject {

	private java.util.ArrayList<Musee> musees;
	private java.util.ArrayList<Salle> salles;
	private java.util.ArrayList<Oeuvre> oeuvres;
	private java.util.ArrayList<Photographiable> photographiables;
	private Utilisateur utilisateurActuel;
	private java.util.ArrayList<Photo> photos;
	private java.util.ArrayList<Utilisateur> utilisateurs;
	private java.util.ArrayList<Demande> demandes;

	private static SystemeLouvre instance;

	private static Scanner scanner;
	
	private SystemeLouvre() {
		super();
		musees = new ArrayList<Musee>();
		salles = new ArrayList<Salle>();
		oeuvres = new ArrayList<Oeuvre>();
		photographiables = new ArrayList<Photographiable>();
		utilisateurs = new ArrayList<Utilisateur>();
		demandes = new ArrayList<Demande>();

		photos = new ArrayList<Photo>();
		
		// Instanciate the scanner for the retrieve of user console inputs
		scanner = new Scanner(System.in);
	}

	public static SystemeLouvre getInstance() {
		if (instance == null)
			instance = new SystemeLouvre();
		return instance;
	}

	public static void resetInstance() {
		instance = new SystemeLouvre();
	}

	public ArrayList<Demande> recupererDemandes() {
		return demandes;
	}

	public void creationDemande()
	{
		try
		{
			Demande demandeARemplir;
			demandeARemplir = getUtilisateurActuel().creationDemande();
			System.out.println("FORMULAIRE DE CREATION DE DEMANDE");
			System.out.println("Liste des objets accessibles pour la demande :");

			String listResumesPh = "";
			for (int idPhotographiable = 0; idPhotographiable < photographiables
					.size(); idPhotographiable++) {
				listResumesPh += idPhotographiable + " : "
						+ photographiables.get(idPhotographiable).recupereResume()
						+ "\n";
			}
			System.out.println(listResumesPh);

			System.out.println("Saisissez le num�ro : ");

			// Instanciate the scanner for the retrieve of user console inputs
			
			int numeroPh = scanner.nextInt();

			System.out.println("Saisissez l'utilisation de la demande :");
			scanner.nextLine();
			String utilisationDemande = scanner.nextLine();

			System.out.println("Saisissez le commentaire de la demande :");
			String commentDemande = scanner.nextLine();

			demandeARemplir.remplir((Historien) getUtilisateurActuel(),
					photographiables.get(numeroPh), utilisationDemande,
					commentDemande);
			
			addDemandes(demandeARemplir);
			System.out.println("Demande enregistr�e.");
			notifier();
		}
		catch (NotAuthorizedException e)
		{
			System.err.println("Vous n'�tes pas autoris� � faire une telle action : "+e.getMessage());
		}
	}

	public void consulterDemandes() {
		try
		{
			ArrayList<Demande> listeDemandes = getUtilisateurActuel().consulterDemandes();
			
			if (listeDemandes.size() == 0)
				System.out.println("Aucune demande existante");
			else{
				System.out.println("Liste des demandes : ");
				int i = 0;
				for (Demande demande : listeDemandes)
				{
					System.out.println(i+" : "+demande);
					i++;
				}
			}
		}
		catch (NotAuthorizedException e) 
		{
			System.err.println("Vous n'�tes pas autoris� � faire une telle action : "+e.getMessage());
		}
	}

	public void afficheMenuActions() {
		ArrayList<Integer> idsActions = getUtilisateurActuel()
				.getActionDisponibles();
		System.out.println("Menu des actions :");
		for (Integer idAction : idsActions) {
			switch (idAction) {
			case Utilisateur.ACTION_CONSULTER_DEMANDES:
				System.out.println(Utilisateur.ACTION_CONSULTER_DEMANDES
						+ " : Consulter les demandes");
				break;
			case Utilisateur.ACTION_CONSULTER_SES_DEMANDES:
				System.out.println(Utilisateur.ACTION_CONSULTER_SES_DEMANDES
						+ " : Consulter ses demandes");
				break;
			case Utilisateur.ACTION_CREER_DEMANDE:
				System.out.println(Utilisateur.ACTION_CREER_DEMANDE
						+ " : Cr�er une demande");
				break;
			case Utilisateur.ACTION_PRENDRE_EN_CHARGE_DEMANDE:
				System.out.println(Utilisateur.ACTION_PRENDRE_EN_CHARGE_DEMANDE
						+ " : Prendre en charge une demande");
				break;
			case Utilisateur.ACTION_COMPLETER_DEMANDE:
				System.out.println(Utilisateur.ACTION_COMPLETER_DEMANDE
						+ " : Completer une demande");
				break;
			case Utilisateur.ACTION_VALIDER_DEMANDE:
				System.out.println(Utilisateur.ACTION_VALIDER_DEMANDE
						+ " : Valider une demande");
				break;
			default:
				break;
			}
		}
		System.out.println("0 : Quitter");
	}

	public boolean connect(String user) {
		for(Utilisateur utilisateur : utilisateurs)
		{
			if(utilisateur.getNom().equals(user))
			{
				this.setUtilisateurActuel(utilisateur);
				System.out.println("Connect�");
				return true;
			}
		}
		System.out.println("L'utilisateur demand� n'existe pas");
		return false;
	}
	
	/** @param idDemande */
	public void prendreEnChargeDemande(int idDemande)
	{
		try
		{
			getUtilisateurActuel().prendreEnChargeDemande(getUtilisateurActuel().consulterDemandes().get(idDemande));
		}
		catch (NotAuthorizedException e)
		{
			System.err.println("vous n'�tes pas autoris� � faire une telle action : "+e.getMessage());
		}
	}

	public void consulterSesDemandes() {
		try
		{
			ArrayList<Demande> demandesAAfficher = getUtilisateurActuel().recupererSesDemandes();
			
			if (demandesAAfficher == null || demandesAAfficher.size() == 0)
				System.out.println("Aucune demande vous concerne directement");
			else{
				System.out.println("Liste de vos demandes : ");
				int i = 0;
				for (Demande demande : demandesAAfficher)
				{
					System.out.println(i+" : "+demande);
					i ++;
				}
			}
		}
		catch (NotAuthorizedException e)
		{
			System.err.println("Vous n'�tes pas autoris� � faire une telle action : "+e.getMessage());
		}
	}

	/** @param idDemande */
	public void validerDemande(int idDemande) {
		try
		{
			getUtilisateurActuel().validerDemande(getUtilisateurActuel().recupererSesDemandes().get(idDemande));
		}
		catch (NotAuthorizedException e)
		{
			System.err.println("Vous n'�tes pas autoris� � faire une telle action : "+e.getMessage());
		}
	}

	/** @param listePhotos */
	public void ajouterAuCatalogue(ArrayList<Photo> listePhotos) {
		photos.addAll(listePhotos);
	}

	/**
	 * @param idDemande
	 * @param listePhotos
	 */
	public void completerDemande(int idDemande, ArrayList<Photo> listePhotos) {
		try
		{
			getUtilisateurActuel().completerDemande(getUtilisateurActuel().recupererSesDemandes().get(idDemande),listePhotos);
		}
		catch (NotAuthorizedException e)
		{
		}
	}

	/** @pdGenerated default getter */
	public ArrayList<Musee> getMusees() {
		if (musees == null)
			musees = new java.util.ArrayList<Musee>();
		return musees;
	}

	/** @pdGenerated default iterator getter */
	public java.util.Iterator<Musee> getIteratorMusees() {
		if (musees == null)
			musees = new java.util.ArrayList<Musee>();
		return musees.iterator();
	}

	/**
	 * @pdGenerated default setter
	 * @param newMusees
	 */
	public void setMusees(ArrayList<Musee> newMusees) {
		removeAllMusees();
		for (java.util.Iterator<Musee> iter = newMusees.iterator(); iter.hasNext();)
			addMusee(iter.next());
	}

	/**
	 * @pdGenerated default add
	 * @param newMusee
	 */
	public void addMusee(Musee newMusee) {
		if (newMusee == null)
			return;
		if (this.musees == null)
			this.musees = new java.util.ArrayList<Musee>();
		if (!this.musees.contains(newMusee))
			this.musees.add(newMusee);
		addPhotographiables(newMusee);
	}

	/**
	 * @pdGenerated default remove
	 * @param oldMusee
	 */
	public void removeMusees(Musee oldMusee) {
		if (oldMusee == null)
			return;
		if (this.musees != null)
			if (this.musees.contains(oldMusee))
				this.musees.remove(oldMusee);
		removePhotographiables(oldMusee);
	}

	/** @pdGenerated default removeAll */
	public void removeAllMusees() {
		if (musees != null)
			musees.clear();
	}

	/** @pdGenerated default getter */
	public ArrayList<Salle> getSalles() {
		if (salles == null)
			salles = new java.util.ArrayList<Salle>();
		return salles;
	}

	/** @pdGenerated default iterator getter */
	public java.util.Iterator<Salle> getIteratorSalles() {
		if (salles == null)
			salles = new java.util.ArrayList<Salle>();
		return salles.iterator();
	}

	/**
	 * @pdGenerated default setter
	 * @param newSalles
	 */
	public void setSalles(ArrayList<Salle> newSalles) {
		removeAllSalles();
		for (java.util.Iterator<Salle> iter = newSalles.iterator(); iter.hasNext();)
			addSalle(iter.next());
	}

	/**
	 * @pdGenerated default add
	 * @param newSalle
	 */
	public void addSalle(Salle newSalle) {
		if (newSalle == null)
			return;
		if (this.salles == null)
			this.salles = new java.util.ArrayList<Salle>();
		if (!this.salles.contains(newSalle))
			this.salles.add(newSalle);
		addPhotographiables(newSalle);
	}

	/**
	 * @pdGenerated default remove
	 * @param oldSalle
	 */
	public void removeSalles(Salle oldSalle) {
		if (oldSalle == null)
			return;
		if (this.salles != null)
			if (this.salles.contains(oldSalle))
				this.salles.remove(oldSalle);
		removePhotographiables(oldSalle);
	}

	/** @pdGenerated default removeAll */
	public void removeAllSalles() {
		if (salles != null)
			salles.clear();
	}

	/** @pdGenerated default getter */
	public ArrayList<Oeuvre> getOeuvres() {
		if (oeuvres == null)
			oeuvres = new java.util.ArrayList<Oeuvre>();
		return oeuvres;
	}

	/** @pdGenerated default iterator getter */
	public java.util.Iterator<Oeuvre> getIteratorOeuvres() {
		if (oeuvres == null)
			oeuvres = new java.util.ArrayList<Oeuvre>();
		return oeuvres.iterator();
	}

	/**
	 * @pdGenerated default setter
	 * @param newOeuvres
	 */
	public void setOeuvres(ArrayList<Oeuvre> newOeuvres) {
		removeAllOeuvres();
		for (java.util.Iterator<Oeuvre> iter = newOeuvres.iterator(); iter.hasNext();)
			addOeuvres(iter.next());
	}

	/**
	 * @pdGenerated default add
	 * @param newOeuvre
	 */
	public void addOeuvres(Oeuvre newOeuvre) {
		if (newOeuvre == null)
			return;
		if (this.oeuvres == null)
			this.oeuvres = new java.util.ArrayList<Oeuvre>();
		if (!this.oeuvres.contains(newOeuvre))
			this.oeuvres.add(newOeuvre);
		addPhotographiables(newOeuvre);
	}

	/**
	 * @pdGenerated default remove
	 * @param oldOeuvre
	 */
	public void removeOeuvres(Oeuvre oldOeuvre) {
		if (oldOeuvre == null)
			return;
		if (this.oeuvres != null)
			if (this.oeuvres.contains(oldOeuvre))
				this.oeuvres.remove(oldOeuvre);
		removePhotographiables(oldOeuvre);
	}

	/** @pdGenerated default removeAll */
	public void removeAllOeuvres() {
		if (oeuvres != null)
			oeuvres.clear();
	}

	/** @pdGenerated default getter */
	public ArrayList<Photographiable> getPhotographiables() {
		if (photographiables == null)
			photographiables = new java.util.ArrayList<Photographiable>();
		return photographiables;
	}

	/** @pdGenerated default iterator getter */
	private java.util.Iterator<Photographiable> getIteratorPhotographiables() {
		if (photographiables == null)
			photographiables = new java.util.ArrayList<Photographiable>();
		return photographiables.iterator();
	}

	/**
	 * @pdGenerated default setter
	 * @param newPhotographiables
	 */
	private void setPhotographiables(ArrayList<Photographiable> newPhotographiables) {
		removeAllPhotographiables();
		for (java.util.Iterator<Photographiable> iter = newPhotographiables.iterator(); iter
				.hasNext();)
			addPhotographiables(iter.next());
	}

	/**
	 * @pdGenerated default add
	 * @param newPhotographiable
	 */
	private void addPhotographiables(Photographiable newPhotographiable) {
		if (newPhotographiable == null)
			return;
		if (this.photographiables == null)
			this.photographiables = new java.util.ArrayList<Photographiable>();
		if (!this.photographiables.contains(newPhotographiable))
			this.photographiables.add(newPhotographiable);
	}

	/**
	 * @pdGenerated default remove
	 * @param oldPhotographiable
	 */
	private void removePhotographiables(Photographiable oldPhotographiable) {
		if (oldPhotographiable == null)
			return;
		if (this.photographiables != null)
			if (this.photographiables.contains(oldPhotographiable))
				this.photographiables.remove(oldPhotographiable);
	}

	/** @pdGenerated default removeAll */
	private void removeAllPhotographiables() {
		if (photographiables != null)
			photographiables.clear();
	}

	/** @pdGenerated default getter */
	public ArrayList<Utilisateur> getUtilisateurs() {
		if (utilisateurs == null)
			utilisateurs = new java.util.ArrayList<Utilisateur>();
		return utilisateurs;
	}

	/** @pdGenerated default iterator getter */
	public java.util.Iterator<Utilisateur> getIteratorUtilisateurs() {
		if (utilisateurs == null)
			utilisateurs = new java.util.ArrayList<Utilisateur>();
		return utilisateurs.iterator();
	}

	/**
	 * @pdGenerated default setter
	 * @param newUtilisateurs
	 */
	public void setUtilisateurs(ArrayList<Utilisateur> newUtilisateurs) {
		removeAllUtilisateurs();
		for (java.util.Iterator<Utilisateur> iter = newUtilisateurs.iterator(); iter
				.hasNext();)
			addUtilisateur(iter.next());
	}

	/**
	 * @pdGenerated default add
	 * @param newUtilisateur
	 */
	public void addUtilisateur(Utilisateur newUtilisateur) {
		if (newUtilisateur == null)
			return;
		if (this.utilisateurs == null)
			this.utilisateurs = new java.util.ArrayList<Utilisateur>();
		if (!this.utilisateurs.contains(newUtilisateur))
			this.utilisateurs.add(newUtilisateur);
	}

	/**
	 * @pdGenerated default remove
	 * @param oldUtilisateur
	 */
	public void removeUtilisateurs(Utilisateur oldUtilisateur) {
		if (oldUtilisateur == null)
			return;
		if (this.utilisateurs != null)
			if (this.utilisateurs.contains(oldUtilisateur))
				this.utilisateurs.remove(oldUtilisateur);
	}

	/** @pdGenerated default removeAll */
	public void removeAllUtilisateurs() {
		if (utilisateurs != null)
			utilisateurs.clear();
	}

	/** @pdGenerated default getter */
	public ArrayList<Demande> getDemandes() {
		if (demandes == null)
			demandes = new java.util.ArrayList<Demande>();
		return demandes;
	}

	/** @pdGenerated default iterator getter */
	public java.util.Iterator<Demande> getIteratorDemandes() {
		if (demandes == null)
			demandes = new java.util.ArrayList<Demande>();
		return demandes.iterator();
	}

	/**
	 * @pdGenerated default setter
	 * @param newDemandes
	 */
	public void setDemandes(ArrayList<Demande> newDemandes) {
		removeAllDemandes();
		for (java.util.Iterator<Demande> iter = newDemandes.iterator(); iter.hasNext();)
			addDemandes(iter.next());
	}

	/**
	 * @pdGenerated default add
	 * @param newDemande
	 */
	public void addDemandes(Demande newDemande) {
		if (newDemande == null)
			return;
		if (this.demandes == null)
			this.demandes = new java.util.ArrayList<Demande>();
		if (!this.demandes.contains(newDemande))
			this.demandes.add(newDemande);
	}

	/**
	 * @pdGenerated default remove
	 * @param oldDemande
	 */
	public void removeDemandes(Demande oldDemande) {
		if (oldDemande == null)
			return;
		if (this.demandes != null)
			if (this.demandes.contains(oldDemande))
				this.demandes.remove(oldDemande);
	}

	/** @pdGenerated default removeAll */
	public void removeAllDemandes() {
		if (demandes != null)
			demandes.clear();
	}

	public Utilisateur getUtilisateurActuel() {
		return utilisateurActuel;
	}

	public void setUtilisateurActuel(Utilisateur utilisateurActuel) {
		this.utilisateurActuel = utilisateurActuel;
	}

	/** @pdGenerated default getter */
	public java.util.List<Photo> getPhotos() {
		if (photos == null)
			photos = new java.util.ArrayList<Photo>();
		return photos;
	}

	/** @pdGenerated default iterator getter */
	public java.util.Iterator<Photo> getIteratorPhotos() {
		if (photos == null)
			photos = new java.util.ArrayList<Photo>();
		return photos.iterator();
	}

	/**
	 * @pdGenerated default setter
	 * @param newPhotos
	 */
	public void setPhotos(java.util.List<Photo> newPhotos) {
		removeAllPhotos();
		for (java.util.Iterator<Photo> iter = newPhotos.iterator(); iter.hasNext();)
			addPhotos(iter.next());
	}

	/**
	 * @pdGenerated default add
	 * @param newPhoto
	 */
	public void addPhotos(Photo newPhoto) {
		if (newPhoto == null)
			return;
		if (this.photos == null)
			this.photos = new java.util.ArrayList<Photo>();
		if (!this.photos.contains(newPhoto))
			this.photos.add(newPhoto);
	}

	/**
	 * @pdGenerated default remove
	 * @param oldPhoto
	 */
	public void removePhotos(Photo oldPhoto) {
		if (oldPhoto == null)
			return;
		if (this.photos != null)
			if (this.photos.contains(oldPhoto))
				this.photos.remove(oldPhoto);
	}

	/** @pdGenerated default removeAll */
	public void removeAllPhotos() {
		if (photos != null)
			photos.clear();
	}

	public static void main(String[] args)
	{
		SystemeLouvre systeme = SystemeLouvre.getInstance();

		IHMDemandes ihm = new IHMDemandes();
		Musee louvre = new Musee();
		louvre.setNom("Louvre");

		Salle salle1 = new Salle();
		salle1.setNom("Salle des sculptures");
		Salle salle2 = new Salle();
		salle2.setNom("Salle des peintures");

		Oeuvre oeuvre1 = new Oeuvre();
		oeuvre1.setNom("Venus de l'Univ");
		Oeuvre oeuvre2 = new Oeuvre();
		oeuvre2.setNom("David");
		Oeuvre oeuvre3 = new Oeuvre();
		oeuvre3.setNom("Joconde");
		Oeuvre oeuvre4 = new Oeuvre();
		oeuvre4.setNom("La Pie");

		Historien hector = new Historien();
		hector.setNom("Hector");
		Historien hommer = new Historien();
		hommer.setNom("Hommer");
		Photographe philippe = new Photographe();
		philippe.setNom("Philippe");

		louvre.addSalle(salle1);
		louvre.addSalle(salle2);

		/*
		 * Inutile car la m�thode addSalle de Musee ajoute la salle dans le
		 * mus�e salle1.setMusee(louvre); salle2.setMusee(louvre);
		 */

		/* La m�thode addOeuvreExpose de Salle ajoute l'oeuvre dans la salle */
		salle1.addOeuvreExpose(oeuvre1);
		salle1.addOeuvreExpose(oeuvre2);
		salle2.addOeuvreExpose(oeuvre3);
		salle2.addOeuvreExpose(oeuvre4);

		/* Ajout des composants dans le syst�me */
		systeme.addMusee(louvre);
		systeme.addSalle(salle1);
		systeme.addSalle(salle2);
		systeme.addUtilisateur(hector);
		systeme.addUtilisateur(hommer);
		systeme.addUtilisateur(philippe);
		
		systeme.addOeuvres(oeuvre1);
		systeme.addOeuvres(oeuvre2);
		systeme.addOeuvres(oeuvre3);
		systeme.addOeuvres(oeuvre4);

		System.out.println("Bienvenue dans le  syst�me");

		boolean connection = true;
		// Declaring an integer witch contains action id
		int idAction;
		while(connection)
		{
			System.out.println("Connexion, saisissez votre nom d'utilisateur ([entrer] pour quitter): ");
			String user = scanner.nextLine();
			boolean running = false;
			
			if(user.isEmpty())
				connection = false;
			else if(systeme.connect(user))
			{
				running = true;
			}
			
			while (running)
			{
				systeme.afficheMenuActions();
				int idDemande;
				idAction = Integer.parseInt(scanner.nextLine());
				
				switch (idAction)
				{
					case Utilisateur.ACTION_CONSULTER_DEMANDES:
						systeme.consulterDemandes();
						break;
					case Utilisateur.ACTION_CONSULTER_SES_DEMANDES:
						systeme.consulterSesDemandes();
						break;
					case Utilisateur.ACTION_CREER_DEMANDE:
						systeme.creationDemande();
						break;
					case Utilisateur.ACTION_PRENDRE_EN_CHARGE_DEMANDE:
						systeme.consulterDemandes();
						System.out.println("Choisissez la demande :");
						idDemande = Integer.parseInt(scanner.nextLine());
						systeme.prendreEnChargeDemande(idDemande);
						break;
					case Utilisateur.ACTION_COMPLETER_DEMANDE:
						systeme.consulterSesDemandes();
						System.out.println("Choisissez la demande � Completer :");
						idDemande = Integer.parseInt(scanner.nextLine());
						ArrayList<Photo> listePhotos = new ArrayList<Photo>();
						String reponse = "test";
						while(!reponse.isEmpty())
						{
							System.out.println("Saisissez le nom de la photo ([entrer] pour quitter) :");
							reponse = scanner.nextLine();
							Photo photo = new Photo();
							photo.setNom(reponse);
							listePhotos.add(photo);
						}
						systeme.completerDemande(idDemande,listePhotos);
						break;
					case Utilisateur.ACTION_VALIDER_DEMANDE:
						systeme.consulterSesDemandes();
						System.out.println("Choisissez la demande :");
						idDemande = Integer.parseInt(scanner.nextLine());
						systeme.validerDemande(idDemande);
						break;
					case 0:
						running = false;
					default:
						break;
				}
			}
		}
	}

}