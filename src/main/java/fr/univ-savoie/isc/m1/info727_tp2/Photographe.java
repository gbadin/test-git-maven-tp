package isc_m1_fw_gb.info727_tp2;

/***********************************************************************
 * Module:  Photographe.java
 * Author:  Gr�goire Badin - Fabien Way
 * Purpose: Defines the Class Photographe
 ***********************************************************************/

import java.util.*;

public class Photographe extends Utilisateur {
   private java.util.ArrayList<Demande> listeDemande;
   
   public ArrayList<Integer> getActionDisponibles() {
		ArrayList<Integer> toReturn = new ArrayList<Integer>();
		toReturn.add(Utilisateur.ACTION_CONSULTER_DEMANDES);
		toReturn.add(Utilisateur.ACTION_CONSULTER_SES_DEMANDES);
		toReturn.add(Utilisateur.ACTION_PRENDRE_EN_CHARGE_DEMANDE);
		toReturn.add(Utilisateur.ACTION_COMPLETER_DEMANDE);
		return toReturn;
	}
   
   public ArrayList<Demande> consulterDemandes() throws NotAuthorizedException{
	   return SystemeLouvre.getInstance().recupererDemandes();
   }
   
   public Demande creationDemande() throws NotAuthorizedException
   {
      throw new NotAuthorizedException("Un photographe ne peut pas cr�er de demande.");
   }
   
   public ArrayList<Demande> recupererSesDemandes() throws NotAuthorizedException{
	   return listeDemande;
   }
   
   /** @param demande */
   public void prendreEnChargeDemande(Demande demande) throws NotAuthorizedException{
      demande.prendreEnCharge(this);
   }
   
   /** @param demande */
   public void validerDemande(Demande demande) throws NotAuthorizedException{
	   throw new NotAuthorizedException("Un Photographe ne peut pas valider de demande.");
   }
   
   /** @param demande 
    * @param listePhotos */
   public void completerDemande(Demande demande, ArrayList<Photo> listePhotos) throws NotAuthorizedException{
      if(listeDemande.contains(demande))
    	  demande.completer(listePhotos);
      else
    	  throw new NotAuthorizedException("Un Photographe ne peut pas completer une demande qu'il ne c'est pas attribu�.");
   }
   
   
   /** @pdGenerated default getter */
   public java.util.List<Demande> getListeDemande() {
      if (listeDemande == null)
         listeDemande = new java.util.ArrayList<Demande>();
      return listeDemande;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator<Demande> getIteratorListeDemande() {
      if (listeDemande == null)
         listeDemande = new java.util.ArrayList<Demande>();
      return listeDemande.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newListeDemande */
   public void setListeDemande(java.util.List<Demande> newListeDemande) {
      removeAllListeDemande();
      for (java.util.Iterator<Demande> iter = newListeDemande.iterator(); iter.hasNext();)
         addListeDemande(iter.next());
   }
   
   /** @pdGenerated default add
     * @param newDemande */
   public void addListeDemande(Demande newDemande) {
      if (newDemande == null)
         return;
      if (this.listeDemande == null)
         this.listeDemande = new java.util.ArrayList<Demande>();
      if (!this.listeDemande.contains(newDemande))
      {
         this.listeDemande.add(newDemande);
         newDemande.setPhotographe(this);      
      }
   }
   
   /** @pdGenerated default remove
     * @param oldDemande */
   public void removeListeDemande(Demande oldDemande) {
      if (oldDemande == null)
         return;
      if (this.listeDemande != null)
         if (this.listeDemande.contains(oldDemande))
         {
            this.listeDemande.remove(oldDemande);
            oldDemande.setPhotographe((Photographe)null);
         }
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllListeDemande() {
      if (listeDemande != null)
      {
         Demande oldDemande;
         for (java.util.Iterator<Demande> iter = getIteratorListeDemande(); iter.hasNext();)
         {
            oldDemande = iter.next();
            iter.remove();
            oldDemande.setPhotographe((Photographe)null);
         }
      }
   }

   public String toString() {
		return "Photographe : " + getNom();
	}
   
}