package isc_m1_fw_gb.info727_tp2;

/***********************************************************************
 * Module:  Utilisateur.java
 * Author:  Gr�goire Badin - Fabien Way
 * Purpose: Defines the Class Utilisateur
 ***********************************************************************/

import java.util.*;

public abstract class Utilisateur {

private String nom;
   
   public String getNom() {
	return nom;
	}
   
	public void setNom(String nom) {
		this.nom = nom;
	}
   public abstract ArrayList<Demande> consulterDemandes() throws NotAuthorizedException;
   public abstract Demande creationDemande() throws NotAuthorizedException;
   public abstract ArrayList<Integer> getActionDisponibles();
   public abstract ArrayList<Demande> recupererSesDemandes() throws NotAuthorizedException;
   /** @param demande */
   public abstract void prendreEnChargeDemande(Demande demande) throws NotAuthorizedException;
   /** @param demande */
   public abstract void validerDemande(Demande demande) throws NotAuthorizedException;
   /** @param demande 
    * @param listePhotos */
   public abstract void completerDemande(Demande demande, ArrayList<Photo> listePhotos) throws NotAuthorizedException;
   
   public static final int ACTION_CONSULTER_DEMANDES = 1;
   public static final int ACTION_CONSULTER_SES_DEMANDES = 2;
   public static final int ACTION_CREER_DEMANDE = 3;
   public static final int ACTION_PRENDRE_EN_CHARGE_DEMANDE = 4;
   public static final int ACTION_COMPLETER_DEMANDE = 5;
   public static final int ACTION_VALIDER_DEMANDE = 6;

}