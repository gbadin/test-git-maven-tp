package isc_m1_fw_gb.info727_tp2;

/***********************************************************************
 * Module:  EtatDemande.java
 * Author:  Gr�goire Badin - Fabien Way
 * Purpose: Defines the Class EtatDemande
 ***********************************************************************/

public abstract class EtatDemande {
   public Demande demande;
   
   /** @param p */
   public abstract boolean prendreEnCharge(Photographe p);
   
   public abstract boolean completer();
   
   public abstract boolean valider();
   
   public EtatDemande(Demande demande)
   {
	   setDemande(demande);
   }
   
   /** @pdGenerated default parent getter */
   public Demande getDemande()
   {
	   return demande;
   }
   
   /** @pdGenerated default parent setter
     * @param newDemande */
   public void setDemande(Demande newDemande) {
      if (this.demande == null || !this.demande.equals(newDemande))
      {
         if (this.demande != null)
            this.demande.setEtat(null);
         this.demande = newDemande;
         if (this.demande != null)
            this.demande.setEtat(this);
      }
   }
}