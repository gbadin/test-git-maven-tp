package isc_m1_fw_gb.info727_tp2;

import java.util.HashSet;

public abstract class Subject
{
	private HashSet<Observer> observers;
	
	Subject()
	{
		super();
		observers = new HashSet<Observer>();
	}
	
	public void attach(Observer observer)
	{
		observers.add(observer);
	}
	public void detach(Observer observer)
	{
		observers.remove(observer);
	}
	
	public void notifier()
	{
		for(Observer observer : observers)
		{
			observer.update();
		}
	}
}
