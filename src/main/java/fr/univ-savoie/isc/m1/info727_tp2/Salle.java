package isc_m1_fw_gb.info727_tp2;

/***********************************************************************
 * Module:  Salle.java
 * Author:  Gr�goire Badin - Fabien Way
 * Purpose: Defines the Class Salle
 ***********************************************************************/

/** @pdOid 84f3f953-f6f0-461a-946b-4c20f4366c14 */
public class Salle implements Photographiable {
   /** @pdOid ba94bdfe-1ff6-4a8c-8d32-44ad42dad1d6 */
   private String nom;
   
   /** @pdRoleInfo migr=no name=Musee assc=association2 mult=1 */
   private Musee musee;
   /** @pdRoleInfo migr=no name=Oeuvre assc=exposeDans coll=java.util.List impl=java.util.ArrayList mult=* side=A */
   private java.util.List<Oeuvre> oeuvresExposes;
   
   /** @pdOid 08a90913-6372-4c17-96ce-2024ef62e873 */
   public String recupereResume() {
	  return "Salle : "+nom;
   }
      
   /** @pdGenerated default parent getter */
   public Musee getMusee() {
      return musee;
   }
   
   /** @pdGenerated default parent setter
     * @param newMusee */
   public void setMusee(Musee newMusee) {
      if (this.musee == null || !this.musee.equals(newMusee))
      {
         if (this.musee != null)
         {
            Musee oldMusee = this.musee;
            this.musee = null;
            oldMusee.removeSalle(this);
         }
         if (newMusee != null)
         {
            this.musee = newMusee;
            this.musee.addSalle(this);
         }
      }
   }
   /** @pdGenerated default getter */
   public java.util.List<Oeuvre> getOeuvresExposes() {
      if (oeuvresExposes == null)
         oeuvresExposes = new java.util.ArrayList<Oeuvre>();
      return oeuvresExposes;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator<Oeuvre> getIteratorOeuvresExposes() {
      if (oeuvresExposes == null)
         oeuvresExposes = new java.util.ArrayList<Oeuvre>();
      return oeuvresExposes.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newOeuvresExposes */
   public void setOeuvresExposes(java.util.List<Oeuvre> newOeuvresExposes) {
      removeAllOeuvresExposees();
      for (java.util.Iterator<Oeuvre> iter = newOeuvresExposes.iterator(); iter.hasNext();)
         addOeuvreExpose(iter.next());
   }
   
   /** @pdGenerated default add
     * @param newOeuvre */
   public void addOeuvreExpose(Oeuvre newOeuvre) {
      if (newOeuvre == null)
         return;
      if (this.oeuvresExposes == null)
         this.oeuvresExposes = new java.util.ArrayList<Oeuvre>();
      if (!this.oeuvresExposes.contains(newOeuvre))
      {
         this.oeuvresExposes.add(newOeuvre);
         newOeuvre.setSalle(this);      
      }
      newOeuvre.setSalle(this);
   }
   
   public String getNom() {
	return nom;
}


public void setNom(String nom) {
	this.nom = nom;
}


/** @pdGenerated default remove
     * @param oldOeuvre */
   public void removeOeuvreExposee(Oeuvre oldOeuvre) {
      if (oldOeuvre == null)
         return;
      if (this.oeuvresExposes != null)
         if (this.oeuvresExposes.contains(oldOeuvre))
         {
            this.oeuvresExposes.remove(oldOeuvre);
            oldOeuvre.setSalle((Salle)null);
         }
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllOeuvresExposees() {
      if (oeuvresExposes != null)
      {
         Oeuvre oldOeuvre;
         for (java.util.Iterator<Oeuvre> iter = getIteratorOeuvresExposes(); iter.hasNext();)
         {
            oldOeuvre = iter.next();
            iter.remove();
            oldOeuvre.setSalle((Salle)null);
         }
      }
   }

}