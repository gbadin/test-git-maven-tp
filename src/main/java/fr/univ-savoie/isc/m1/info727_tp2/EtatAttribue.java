package isc_m1_fw_gb.info727_tp2;

/***********************************************************************
 * Module:  EtatAttribue.java
 * Author:  Gr�goire Badin - Fabien Way
 * Purpose: Defines the Class EtatAttribue
 ***********************************************************************/

public class EtatAttribue extends EtatDemande {
  
	public EtatAttribue(Demande demande) {
		super(demande);
	}

/** @param p */
   public boolean prendreEnCharge(Photographe p) {
      return false;
   }
   
   public boolean completer() {
      demande.setEtat(new EtatComplete(demande));
      return true;
   }
   
   public boolean valider() {
      return false;
   }
   
   public String toString() {
	   return "Demande attribu�e au photographe " + demande.getPhotographe().getNom();
   }

}