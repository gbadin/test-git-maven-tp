package isc_m1_fw_gb.info727_tp1;
/***********************************************************************
 * Module:  Utilisateur.java
 * Author:  Gr�goire Badin - Fabien Way
 * Purpose: Defines the Class Utilisateur
 ***********************************************************************/

import java.util.*;

/** @pdOid 41b53cde-a7f5-43f9-a1a3-36767ed633b9 */
public abstract class Utilisateur {
   /** @pdOid c826bcd5-cfa7-457e-a227-49e9f03ee3d8 */
   public abstract ArrayList<Demande> consulterDemandes();
   /** @pdOid 852ecbfa-b292-4002-bb10-4c0d0aab760e */
   public abstract Demande creationDemande();
   /** @pdOid 3ce2ab22-721f-4e3e-94fa-8cc84c004675 */
   public abstract ArrayList<Integer> getActionDisponibles();
   
   private String nom;
   
   public String getNom() {
	return nom;
	}
   
	public void setNom(String nom) {
		this.nom = nom;
	}

public static final int ACTION_CONSULTER_DEMANDES = 1;
   public static final int ACTION_CREER_DEMANDE = 2;

}