package isc_m1_fw_gb.info727_tp1;
/***********************************************************************
 * Module:  Musee.java
 * Author:  Gr�goire Badin - Fabien Way
 * Purpose: Defines the Class Musee
 ***********************************************************************/

import java.util.*;

/** @pdOid 7781549b-7cee-4925-a034-e302ba88be66 */
public class Musee implements Photographiable {
   /** @pdOid 25eb7b7b-b6a6-4ff2-9811-7c9daef28749 */
   private String nom;
   
   public String getNom() {
	return nom;
}


public void setNom(String nom) {
	this.nom = nom;
}

/** @pdRoleInfo migr=no name=Salle assc=association2 coll=java.util.List impl=java.util.ArrayList mult=0..* side=A */
   public java.util.List<Salle> salles;
   
   /** @pdOid 9a1d2ea4-938f-41a6-89ed-5fca58d0a63f */
   public String recupereResume() {
	   return "Musee : "+nom;
   }
   
   /** @pdGenerated default getter */
   public java.util.List<Salle> getSalles() {
      if (salles == null)
         salles = new java.util.ArrayList<Salle>();
      return salles;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorSalles() {
      if (salles == null)
         salles = new java.util.ArrayList<Salle>();
      return salles.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newSalles */
   public void setSalles(java.util.List<Salle> newSalles) {
      removeAllSalles();
      for (java.util.Iterator iter = newSalles.iterator(); iter.hasNext();)
         addSalle((Salle)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newSalle */
   public void addSalle(Salle newSalle) {
      if (newSalle == null)
         return;
      if (this.salles == null)
         this.salles = new java.util.ArrayList<Salle>();
      if (!this.salles.contains(newSalle))
      {
         this.salles.add(newSalle);
         newSalle.setMusee(this);      
      }
      newSalle.setMusee(this);
   }
   
   /** @pdGenerated default remove
     * @param oldSalle */
   public void removeSalle(Salle oldSalle) {
      if (oldSalle == null)
         return;
      if (this.salles != null)
         if (this.salles.contains(oldSalle))
         {
            this.salles.remove(oldSalle);
            oldSalle.setMusee((Musee)null);
         }
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllSalles() {
      if (salles != null)
      {
         Salle oldSalle;
         for (java.util.Iterator iter = getIteratorSalles(); iter.hasNext();)
         {
            oldSalle = (Salle)iter.next();
            iter.remove();
            oldSalle.setMusee((Musee)null);
         }
      }
   }

}