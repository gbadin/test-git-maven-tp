package isc_m1_fw_gb.info727_tp1;
/***********************************************************************
 * Module:  SystemeLouvre.java
 * Author:  Gr�goire Badin - Fabien Way
 * Purpose: Defines the Class SystemeLouvre
 ***********************************************************************/

import java.util.*;

/** @pdOid 6da6d999-1097-47eb-a071-98d1adc58e2d */
public class SystemeLouvre {
	/** @pdRoleInfo migr=no name=Musee assc=gere coll=ArrayList impl=java.util.ArrayList mult=0..* */
	private ArrayList<Musee> musees;
	/** @pdRoleInfo migr=no name=Salle assc=association6 coll=ArrayList impl=java.util.ArrayList mult=0..* */
	private ArrayList<Salle> salles;
	/** @pdRoleInfo migr=no name=Oeuvre assc=association7 coll=ArrayList impl=java.util.ArrayList mult=0..* */
	private ArrayList<Oeuvre> oeuvres;
	/** @pdRoleInfo migr=no name=Photographiable assc=connait coll=ArrayList impl=java.util.ArrayList mult=0..* */
	private ArrayList<Photographiable> photographiables;
	/** @pdRoleInfo migr=no name=Utilisateur assc=utilise mult=0..1 */
	private Utilisateur utilisateurActuel;
	
	/** @pdRoleInfo migr=no name=Utilisateur assc=association3 coll=ArrayList impl=java.util.ArrayList mult=0..* side=A */
	private ArrayList<Utilisateur> utilisateurs;
	/** @pdRoleInfo migr=no name=Demande assc=association4 coll=ArrayList impl=java.util.ArrayList mult=0..* side=A */
	private ArrayList<Demande> demandes;
   
	private static SystemeLouvre instance;
	
	private SystemeLouvre()
	{
		musees = new ArrayList<Musee>();
		salles = new ArrayList<Salle>();
		oeuvres = new ArrayList<Oeuvre>();
		photographiables = new ArrayList<Photographiable>();
		utilisateurs = new ArrayList<Utilisateur>();
		demandes  = new ArrayList<Demande>();
	}
	
   /** @pdOid 6d4ed958-be83-4b4b-9dda-0d595a602b98 */
   public void consulterDemandes()
   {
	   System.out.println("Liste des demandes : ");
	   for(Demande demande : getUtilisateurActuel().consulterDemandes())
	   {
		   System.out.println(demande);
	   }
   }
   
   /** @pdOid 2d06fa57-b79b-460d-ba8e-f1163ef283df */
   public void creationDemande() {
	   Demande demandeARemplir = getUtilisateurActuel().creationDemande();
	   
	   // Exception � g�rer au cas o� l'utilisateur n'a pas le droit
	   
	   System.out.println("FORMULAIRE DE CREATION DE DEMANDE");
	   System.out.println("Liste des objets accessibles pour la demande :");
	   
	   String listResumesPh = "";
	   for (int idPhotographiable = 0 ; idPhotographiable < photographiables.size() ; idPhotographiable++){
		   listResumesPh+= idPhotographiable + " : " 
				   + photographiables.get(idPhotographiable).recupereResume() + "\n";
	   }
	   System.out.println(listResumesPh);
	   
	   
	   System.out.println("Saisissez le num�ro : ");
	   
	   //Instanciate the scanner for the retrieve of user console inputs
	   Scanner scanner = new Scanner(System.in);
	   int numeroPh = scanner.nextInt();
	   
	   System.out.println("Saisissez l'utilisation de la demande :");
	   scanner.nextLine();
	   String utilisationDemande = scanner.nextLine();
	   
	   System.out.println("Saisissez le commentaire de la demande :");
	   String commentDemande = scanner.nextLine();
	   
	   demandeARemplir.remplir((Historien)getUtilisateurActuel(), photographiables.get(numeroPh), utilisationDemande, commentDemande);
	   addDemandes(demandeARemplir);
	   System.out.println("Demande enregistr�e.");
   }
   
   /** @param cible 
    * @param utilisation 
    * @param commentaires
    * @pdOid df903bba-6fef-4941-8318-a15d2c1bd4aa */
   public void enregistrerFormulaire(Photographiable cible, String utilisation, String commentaire) {
	   
   }
   
   public ArrayList<Demande> recupererDemandes()
   {
	  return demandes;
   }
   
   public void afficheMenuActions()
   {
	   ArrayList<Integer> idsActions = getUtilisateurActuel().getActionDisponibles();
	   System.out.println("Menu des actions :");
	   for(Integer idAction : idsActions)
	   {
		   switch(idAction)
		   {
		   	case Utilisateur.ACTION_CONSULTER_DEMANDES:
		   		System.out.println(Utilisateur.ACTION_CONSULTER_DEMANDES+" : Consulter les demandes");
		   		break;
		   	case Utilisateur.ACTION_CREER_DEMANDE:
		   		System.out.println(Utilisateur.ACTION_CREER_DEMANDE+" : Cr�er une demande");
		   		break;
		   default:
			   break;
		   }
	   }
	   System.out.println("0 : Quitter");
   }
   
   /** @pdOid f69c6c31-3d59-42bf-8be5-076e7d1b247e */
   public static SystemeLouvre getInstance() {
      if(instance == null)
    	  instance = new SystemeLouvre();
      return instance;
   }
   
   /** @pdOid c5962354-26c9-4062-b09b-ffd5670eaf61 */
   public static void resetInstance() {
	   instance = new SystemeLouvre();
   }
   
   
   /** @pdGenerated default getter */
   public ArrayList<Musee> getMusees() {
      if (musees == null)
         musees = new java.util.ArrayList<Musee>();
      return musees;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorMusees() {
      if (musees == null)
         musees = new java.util.ArrayList<Musee>();
      return musees.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newMusees */
   public void setMusees(ArrayList<Musee> newMusees) {
      removeAllMusees();
      for (java.util.Iterator iter = newMusees.iterator(); iter.hasNext();)
         addMusee((Musee)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newMusee */
   public void addMusee(Musee newMusee) {
      if (newMusee == null)
         return;
      if (this.musees == null)
         this.musees = new java.util.ArrayList<Musee>();
      if (!this.musees.contains(newMusee))
         this.musees.add(newMusee);
      addPhotographiables(newMusee);
   }
   
   /** @pdGenerated default remove
     * @param oldMusee */
   public void removeMusees(Musee oldMusee) {
      if (oldMusee == null)
         return;
      if (this.musees != null)
         if (this.musees.contains(oldMusee))
            this.musees.remove(oldMusee);
      removePhotographiables(oldMusee);
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllMusees() {
      if (musees != null)
         musees.clear();
   }
   /** @pdGenerated default getter */
   public ArrayList<Salle> getSalles() {
      if (salles == null)
         salles = new java.util.ArrayList<Salle>();
      return salles;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorSalles() {
      if (salles == null)
         salles = new java.util.ArrayList<Salle>();
      return salles.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newSalles */
   public void setSalles(ArrayList<Salle> newSalles) {
      removeAllSalles();
      for (java.util.Iterator iter = newSalles.iterator(); iter.hasNext();)
         addSalle((Salle)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newSalle */
   public void addSalle(Salle newSalle) {
      if (newSalle == null)
         return;
      if (this.salles == null)
         this.salles = new java.util.ArrayList<Salle>();
      if (!this.salles.contains(newSalle))
         this.salles.add(newSalle);
      addPhotographiables(newSalle);
   }
   
   /** @pdGenerated default remove
     * @param oldSalle */
   public void removeSalles(Salle oldSalle) {
      if (oldSalle == null)
         return;
      if (this.salles != null)
         if (this.salles.contains(oldSalle))
            this.salles.remove(oldSalle);
      removePhotographiables(oldSalle);
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllSalles() {
      if (salles != null)
         salles.clear();
   }
   /** @pdGenerated default getter */
   public ArrayList<Oeuvre> getOeuvres() {
      if (oeuvres == null)
         oeuvres = new java.util.ArrayList<Oeuvre>();
      return oeuvres;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorOeuvres() {
      if (oeuvres == null)
         oeuvres = new java.util.ArrayList<Oeuvre>();
      return oeuvres.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newOeuvres */
   public void setOeuvres(ArrayList<Oeuvre> newOeuvres) {
      removeAllOeuvres();
      for (java.util.Iterator iter = newOeuvres.iterator(); iter.hasNext();)
         addOeuvres((Oeuvre)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newOeuvre */
   public void addOeuvres(Oeuvre newOeuvre) {
      if (newOeuvre == null)
         return;
      if (this.oeuvres == null)
         this.oeuvres = new java.util.ArrayList<Oeuvre>();
      if (!this.oeuvres.contains(newOeuvre))
         this.oeuvres.add(newOeuvre);
      addPhotographiables(newOeuvre);
   }
   
   /** @pdGenerated default remove
     * @param oldOeuvre */
   public void removeOeuvres(Oeuvre oldOeuvre) {
      if (oldOeuvre == null)
         return;
      if (this.oeuvres != null)
         if (this.oeuvres.contains(oldOeuvre))
            this.oeuvres.remove(oldOeuvre);
      removePhotographiables(oldOeuvre);
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllOeuvres() {
      if (oeuvres != null)
         oeuvres.clear();
   }
   /** @pdGenerated default getter */
   public ArrayList<Photographiable> getPhotographiables() {
      if (photographiables == null)
         photographiables = new java.util.ArrayList<Photographiable>();
      return photographiables;
   }
   
   /** @pdGenerated default iterator getter */
   private java.util.Iterator getIteratorPhotographiables() {
      if (photographiables == null)
         photographiables = new java.util.ArrayList<Photographiable>();
      return photographiables.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newPhotographiables */
   private void setPhotographiables(ArrayList<Photographiable> newPhotographiables) {
      removeAllPhotographiables();
      for (java.util.Iterator iter = newPhotographiables.iterator(); iter.hasNext();)
         addPhotographiables((Photographiable)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newPhotographiable */
   private void addPhotographiables(Photographiable newPhotographiable) {
      if (newPhotographiable == null)
         return;
      if (this.photographiables == null)
         this.photographiables = new java.util.ArrayList<Photographiable>();
      if (!this.photographiables.contains(newPhotographiable))
         this.photographiables.add(newPhotographiable);
   }
   
   /** @pdGenerated default remove
     * @param oldPhotographiable */
   private void removePhotographiables(Photographiable oldPhotographiable) {
      if (oldPhotographiable == null)
         return;
      if (this.photographiables != null)
         if (this.photographiables.contains(oldPhotographiable))
            this.photographiables.remove(oldPhotographiable);
   }
   
   /** @pdGenerated default removeAll */
   private void removeAllPhotographiables() {
      if (photographiables != null)
         photographiables.clear();
   }
   /** @pdGenerated default getter */
   public ArrayList<Utilisateur> getUtilisateurs() {
      if (utilisateurs == null)
         utilisateurs = new java.util.ArrayList<Utilisateur>();
      return utilisateurs;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorUtilisateurs() {
      if (utilisateurs == null)
         utilisateurs = new java.util.ArrayList<Utilisateur>();
      return utilisateurs.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newUtilisateurs */
   public void setUtilisateurs(ArrayList<Utilisateur> newUtilisateurs) {
      removeAllUtilisateurs();
      for (java.util.Iterator iter = newUtilisateurs.iterator(); iter.hasNext();)
         addUtilisateur((Utilisateur)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newUtilisateur */
   public void addUtilisateur(Utilisateur newUtilisateur) {
      if (newUtilisateur == null)
         return;
      if (this.utilisateurs == null)
         this.utilisateurs = new java.util.ArrayList<Utilisateur>();
      if (!this.utilisateurs.contains(newUtilisateur))
         this.utilisateurs.add(newUtilisateur);
   }
   
   /** @pdGenerated default remove
     * @param oldUtilisateur */
   public void removeUtilisateurs(Utilisateur oldUtilisateur) {
      if (oldUtilisateur == null)
         return;
      if (this.utilisateurs != null)
         if (this.utilisateurs.contains(oldUtilisateur))
            this.utilisateurs.remove(oldUtilisateur);
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllUtilisateurs() {
      if (utilisateurs != null)
         utilisateurs.clear();
   }
   /** @pdGenerated default getter */
   public ArrayList<Demande> getDemandes() {
      if (demandes == null)
         demandes = new java.util.ArrayList<Demande>();
      return demandes;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorDemandes() {
      if (demandes == null)
         demandes = new java.util.ArrayList<Demande>();
      return demandes.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newDemandes */
   public void setDemandes(ArrayList<Demande> newDemandes) {
      removeAllDemandes();
      for (java.util.Iterator iter = newDemandes.iterator(); iter.hasNext();)
         addDemandes((Demande)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newDemande */
   public void addDemandes(Demande newDemande) {
      if (newDemande == null)
         return;
      if (this.demandes == null)
         this.demandes = new java.util.ArrayList<Demande>();
      if (!this.demandes.contains(newDemande))
         this.demandes.add(newDemande);
   }
   
   /** @pdGenerated default remove
     * @param oldDemande */
   public void removeDemandes(Demande oldDemande) {
      if (oldDemande == null)
         return;
      if (this.demandes != null)
         if (this.demandes.contains(oldDemande))
            this.demandes.remove(oldDemande);
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllDemandes() {
      if (demandes != null)
         demandes.clear();
   }
   
   public Utilisateur getUtilisateurActuel() {
		return utilisateurActuel;
   }

   public void setUtilisateurActuel(Utilisateur utilisateurActuel) {
		this.utilisateurActuel = utilisateurActuel;
   }


   public static void main(String[] args){
	   SystemeLouvre systeme = SystemeLouvre.getInstance();
	   
	   Musee louvre = new Musee();
	   louvre.setNom("Louvre");
	   
	   Salle salle1 = new Salle();
	   salle1.setNom("Salle des sculptures");
	   Salle salle2 = new Salle();
	   salle2.setNom("Salle des peintures");
	   
	   Oeuvre oeuvre1 = new Oeuvre();
	   oeuvre1.setNom("Venus de l'Univ");
	   Oeuvre oeuvre2 = new Oeuvre();
	   oeuvre2.setNom("David");
	   Oeuvre oeuvre3 = new Oeuvre();
	   oeuvre3.setNom("Joconde");
	   Oeuvre oeuvre4 = new Oeuvre();
	   oeuvre4.setNom("La Pie");
	   
	   Historien greg = new Historien();
	   greg.setNom("Gr�goire");
	   Historien fabien = new Historien();
	   fabien.setNom("Fabien");
	   
	   louvre.addSalle(salle1);
	   louvre.addSalle(salle2);
	   
	   /* Inutile car la m�thode addSalle de Musee ajoute la salle dans le mus�e 
	    * salle1.setMusee(louvre); 
	   salle2.setMusee(louvre);*/ 
	   
	   /* La m�thode addOeuvreExpose de Salle ajoute l'oeuvre dans la salle */
	   salle1.addOeuvreExpose(oeuvre1);
	   salle1.addOeuvreExpose(oeuvre2);
	   salle2.addOeuvreExpose(oeuvre3);
	   salle2.addOeuvreExpose(oeuvre4);
	   
	   /* Ajout des composants dans le syst�me */
	   systeme.addMusee(louvre);
	   systeme.addSalle(salle1);
	   systeme.addSalle(salle2);
	   systeme.addUtilisateur(greg);
	   systeme.addUtilisateur(fabien);
	   systeme.setUtilisateurActuel(greg); // Normalement r�alis� par l'authentification
	   systeme.addOeuvres(oeuvre1);
	   systeme.addOeuvres(oeuvre2);
	   systeme.addOeuvres(oeuvre3);
	   systeme.addOeuvres(oeuvre4);
	   

	   System.out.println("Bienvenue dans le  syst�me");
	   
	   boolean running = true;
	   //Declaring int wich contains action id
	   int idAction;
	   
	   //Instanciate the scanner for the retrieve of user console inputs
   		Scanner scanner = new Scanner(System.in);
	   
	   while(running)
	   {
		   systeme.afficheMenuActions();
		   idAction = scanner.nextInt();
		   
		   switch(idAction)
		   {
			   case Utilisateur.ACTION_CONSULTER_DEMANDES:
				   systeme.consulterDemandes();
				   break;
			   case Utilisateur.ACTION_CREER_DEMANDE:
				   systeme.creationDemande();
				   break;
			   case 0 :
				   running = false;
			   default:
				   break;
		   }
	   }
	   
	   
	   
	   
	   
	   
   }
}