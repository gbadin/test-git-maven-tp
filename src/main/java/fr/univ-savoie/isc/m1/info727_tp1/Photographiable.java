package isc_m1_fw_gb.info727_tp1;
/***********************************************************************
 * Module:  Photographiable.java
 * Author:  Gr�goire Badin - Fabien Way
 * Purpose: Defines the Interface Photographiable
 ***********************************************************************/

import java.util.*;

/** @pdOid 3d302b97-9451-4ac7-99f6-978f65d1059c */
public interface Photographiable {
   /** @pdOid 19939712-4137-48be-97a7-e6160d6b829b */
   public String recupereResume();

}