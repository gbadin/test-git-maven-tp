package isc_m1_fw_gb.info727_tp1;
/***********************************************************************
 * Module:  Demande.java
 * Author:  Gr�goire Badin - Fabien Way
 * Purpose: Defines the Class Demande
 ***********************************************************************/

import java.util.*;

public class Demande {
   
   private Photographiable photographiable;
   private Historien historien;
   private String utilisation;
   private String commentaire;
   
   public void remplir(Historien historien, Photographiable cible, String utilisation, String commentaire) {
	   photographiable = cible;
	   this.historien = historien;
	   this.utilisation = utilisation;
	   this.commentaire = commentaire;
   }
   
   /** @pdGenerated default parent getter */
   public Historien getHistorien() {
      return historien;
   }
   
   /** @pdGenerated default parent setter
     * @param newHistorien */
   public void setHistorien(Historien newHistorien) {
      if (this.historien == null || !this.historien.equals(newHistorien))
      {
         if (this.historien != null)
         {
            Historien oldHistorien = this.historien;
            this.historien = null;
            oldHistorien.removeDemande(this);
         }
         if (newHistorien != null)
         {
            this.historien = newHistorien;
            this.historien.addDemande(this);
         }
      }
      
      
   }
   
	public String toString() {
		return "Objet concern� : " + photographiable.recupereResume() + ", "
				+ historien + ",	utilisation : " + utilisation + ",	commentaire : " + commentaire ;
	}

}