package isc_m1_fw_gb.info727_tp1;

/***********************************************************************
 * Module:  Oeuvre.java
 * Author:  fabien
 * Purpose: Defines the Class Oeuvre
 ***********************************************************************/

import java.util.*;

/** @pdOid 53fdf9fb-9367-4c63-8738-97519eefb32d */
public class Oeuvre implements Photographiable {
   /** @pdOid cbce6310-f397-4e3c-9018-c5ccfe1a4ce4 */
   private String nom;
   
   public String getNom() {
	return nom;
}


public void setNom(String nom) {
	this.nom = nom;
}

/** @pdRoleInfo migr=no name=Salle assc=exposeDans mult=0..1 */
   public Salle salle;
   
   /** @pdOid e87aa261-377a-48ea-9a36-8bb124b2737f */
   public String recupereResume() {
      return "Oeuvre : "+nom;
   }
   
   
   /** @pdGenerated default parent getter */
   public Salle getSalle() {
      return salle;
   }
   
   /** @pdGenerated default parent setter
     * @param newSalle */
   public void setSalle(Salle newSalle) {
      if (this.salle == null || !this.salle.equals(newSalle))
      {
         if (this.salle != null)
         {
            Salle oldSalle = this.salle;
            this.salle = null;
            oldSalle.removeOeuvreExposee(this);
         }
         if (newSalle != null)
         {
            this.salle = newSalle;
            this.salle.addOeuvreExpose(this);
         }
      }
   }

}