package isc_m1_fw_gb.info727_tp1;
/***********************************************************************
 * Module:  Historien.java
 * Author:  Gr�goire Badin - Fabien Way
 * Purpose: Defines the Class Historien
 ***********************************************************************/

import java.util.*;

/** @pdOid 8c7b297e-6eb9-4cf2-91c1-64962e4a142d */
public class Historien extends Utilisateur {
   /** @pdRoleInfo migr=no name=Demande assc=gere coll=java.util.List impl=java.util.ArrayList mult=0..* */
	private java.util.List<Demande> demandes;
   
   /** @pdOid bbc352c7-1772-4eab-83ce-bf08df45647b */
   public ArrayList<Demande> consulterDemandes()
   {
      return SystemeLouvre.getInstance().recupererDemandes();
   }
   
   /** @pdOid 2195ce40-22cc-4894-b299-5b5328c77911 */
   public Demande creationDemande() {
      return new Demande();
   }
   
   /** @pdOid 04c96589-eecf-4dbe-afff-4e9dfe682709 */
   public ArrayList<Integer> getActionDisponibles() {
	   ArrayList<Integer> toReturn = new ArrayList<Integer>();
	   toReturn.add(Utilisateur.ACTION_CONSULTER_DEMANDES);
	   toReturn.add(Utilisateur.ACTION_CREER_DEMANDE);
	   return toReturn;
   }
   
   
   /** @pdGenerated default getter */
   public java.util.List<Demande> getDemandes() {
      if (demandes == null)
         demandes = new java.util.ArrayList<Demande>();
      return demandes;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorDemandes() {
      if (demandes == null)
         demandes = new java.util.ArrayList<Demande>();
      return demandes.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newDemandes */
   public void setDemandes(java.util.List<Demande> newDemandes) {
      removeAllDemandes();
      for (java.util.Iterator iter = newDemandes.iterator(); iter.hasNext();)
         addDemande((Demande)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newDemande */
   public void addDemande(Demande newDemande) {
      if (newDemande == null)
         return;
      if (this.demandes == null)
         this.demandes = new java.util.ArrayList<Demande>();
      if (!this.demandes.contains(newDemande))
      {
         this.demandes.add(newDemande);
         newDemande.setHistorien(this);      
      }
   }
   
   /** @pdGenerated default remove
     * @param oldDemande */
   public void removeDemande(Demande oldDemande) {
      if (oldDemande == null)
         return;
      if (this.demandes != null)
         if (this.demandes.contains(oldDemande))
         {
            this.demandes.remove(oldDemande);
            oldDemande.setHistorien((Historien)null);
         }
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllDemandes() {
      if (demandes != null)
      {
         Demande oldDemande;
         for (java.util.Iterator iter = getIteratorDemandes(); iter.hasNext();)
         {
            oldDemande = (Demande)iter.next();
            iter.remove();
            oldDemande.setHistorien((Historien)null);
         }
      }
   }
   
   public String toString() {
		return "Historien : " +getNom() ;
	}
}